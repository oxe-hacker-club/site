title: OHC

A 0xE Hacker Conference é uma conferência dedicada ao compartilhamento do conhecimento sobre hacking e segurança entre estudantes, entusiastas e profissionais da área. Numa noite repleta de palestras e discussões sobre hacking você terá uma oportunidade única para aprender sobre as últimas tendências, técnicas e ferramentas utilizadas no mundo do hacking, além de participar de um ambiente de networking entre os participantes.

A segunda edição da 0xE Hacker Conference será realizado no sábado, 05 de abril de 2025, a partir das 16h, no Maceió Mar Hotel. Não perca a chance de expandir seus conhecimentos e se conectar com a comunidade hacker em Maceió.

Clique [aqui](https://conference.oxehc.com.br/) e acesse o hotsite do evento para mais informações!