# Site oficial - 0x3 Hacker Club

Este é o repositório do site oficial do 0xE Hacker Club.

## Guia de desenvolvivemnto

### Instalação
Efetue a montagem do ambiente e instalação das dependências com:
```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.pip
```

### Build

```
make html
```

### Execução

```
make serve
```